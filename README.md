Clojure-Sleuth
==========

A Clojure clone of the ASCII murder mystery game [sleuth].
[sleuth]: http://en.wikipedia.org/wiki/Sleuth_(video_game)


`clojure-sleuth` uses my clojure [bindings] to the [libtcod] library.

[libtcod]: http://doryen.eptalys.net/libtcod/

[bindings]: https://github.com/cdelorey/clj-libtcod.git


License
--------
Distributed under the Eclipse Public License.
